
pub mod bit_settings {
    pub fn sha1_populate() -> Vec<&'static str> {
        let sha1sum_check: Vec<&'static str> = vec!["9aeeb0dcec7602d915f22e6eb8d5a81c78c86ef7",
                                                    "97968778dd926d1814f84c733fae8fc55b73a6e8",
                                                    "606a21de86c1b352c6126de515ba5d5051c384cc",
                                                    "3c21716982d092857537791b689d4071e94318ca",
                                                    "a4a8969de326de1c8c612ec54801b32783d86791",
                                                    "3040c80ba77e195701807fdcacf89e07aa6a5b4b",
                                                    "9af10b6d3888e6ae184a277576ba256bd2387dae",
                                                    "b419f57031b53bb7aa20870b3e56843951683338",
                                                    "05055c9f95b8252882b241dbb663b14311ff3357",
                                                    "5734089e6fe47832d2fa67bd2e59f537146822ca",
                                                    "68209a4efbc005b56611f15c90cfa287bad6cb2a",
                                                    "6606cd773aa5895aff2c567ed8637985df610ed1"];
        return sha1sum_check;
    }
    pub fn md5_populate() -> Vec<String> {
        let md5sum_check: Vec<String> = vec!["6a84966bba3066216712988f7e957619".to_string(),
                                             "68e842addd20020fd7ed89aad249684e".to_string(),
                                             "3d925d24ca8d8431e573b1ab79fb1a2f".to_string(),
                                             "35a4e2e5004e68c846b5f544a4dab588".to_string(),
                                             "d6db77d41a2a436b2d7842030f187bf6".to_string(),
                                             "d6d6258e9ddc5d91918c4e35c6b46946".to_string(),
                                             "a68954987fb1267054cec0f6cd8a204b".to_string(),
                                             "66b639f2e600122c96a699362ea335fc".to_string(),
                                             "7bff77209e9db80b1cf97d69fa847f3a".to_string(),
                                             "21574fc7203bb79f8346b17079906497".to_string(),
                                             "8a17ea07ecb0381bfbacd66f4dea783c".to_string(),
                                             "7865a89068419748beb31e709c88332d".to_string()];
        return md5sum_check;
    }


    pub fn labs_releases() -> Vec<&'static str> {
        let labs_release_populate: Vec<&'static str> = vec!["archlabs-2018-05.iso.torrent",
                                                            "archlabs-2018-05.iso",
                                                            "archlabs-2018-03.iso.torrent",
                                                            "archlabs-2018-03.iso",
                                                            "archlabs-2018-02.iso.torrent",
                                                            "archlabs-2018-02.iso",
                                                            "archlabs-2017-12.iso.torrent",
                                                            "archlabs-2017-12.iso",
                                                            "archlabs-2017-10.iso.torrent",
                                                            "archlabs-2017-10.iso",
                                                            "archlabs-2017-10-lts.iso",
                                                            "archlabs-2017-10-lts.iso.torrent"];
        return labs_release_populate;
    }
    pub fn not_found() -> &'static str {
        let error_file: &'static str = "No compatible ISO or Torrent files were found \
        in your home directory tree";
        return error_file;
    }
}
