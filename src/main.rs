extern crate home;
extern crate walkdir;
extern crate sha1;
extern crate md5;
use walkdir::WalkDir;
use std::env::home_dir;
use std::process::exit;
use std::iter::Iterator;
use std::io::Error;

mod bit_setting;


fn main() {

    let labs_version = labs_filename();

    if labs_version.eq("Empty") {
        println!("{}", bit_setting::bit_settings::not_found());
        exit(1);
    } else {
        println!("{}", &labs_version);
    }

    'outer: for entry in WalkDir::new
        (home_dir().unwrap()).into_iter().filter_map
    (|e| e.ok()) {
        if entry.path().file_name().unwrap().eq(labs_version) {
            check_sha1(entry.path().display().to_string());
            check_md5(entry.path().display().to_string());
        }
    }
    /*Consider changing to return a vector of vaules
      to be parsed out in the main body.
      */
    fn labs_filename<'a>() -> &'a str {
        let mut labs_filename: &str = "Empty";
        'outer_walk: for entry in WalkDir::new
            (home_dir().unwrap()).into_iter().filter_map(|e| e.ok()) {
            let labs_releases = bit_setting::bit_settings::labs_releases();
            'outer: for release in labs_releases {
                'inner_walk: while entry.path().file_name().unwrap().eq(release) {
                    labs_filename = release;
                    break 'outer_walk;
                }
            }
        }
        labs_filename
    }
    fn check_sha1(a:String){
        use sha1::Sha1;
        use sha1::Digest;
        use std::fs::File;
        use std::io::prelude::*;
        let labs_file = a;
        println!("{:?}", &labs_file);
        let mut file = File::open(&labs_file.to_string()).expect("Failed to Open File!");
        let mut data = Vec::new();
        let _result = File::read_to_end(&mut file,&mut data);
        let mut hash = Sha1::default();
        hash.input(&data);
        let output = hash.result();
        let hash_result = String::from(format!("{:x}", &output));

        for hex in bit_setting::bit_settings::sha1_populate(){
            if hex.eq(&hash_result){
                println!("SHA1:\t{}\nTEST:\t{}", hex, hash_result);
                println!("SHA1 Check: PASS");
            }
        }
    }
    fn check_md5(a:String){
        use md5::Md5;
        use md5::Digest;
        use std::fs::File;
        use std::io::prelude::*;
        let labs_file:String = a;
        let mut file:File = File::open(&labs_file.to_string()).expect("Failed to Open File!");
        let mut data:Vec<u8> = Vec::new();
        let _result:Result<usize, Error> = File::read_to_end(&mut file, &mut data);
        let mut hash:Md5 = Md5::default();
        hash.input(&data);
        let output = hash.result();
        let hash_result = String::from(format!("{:x}", &output));

        for hex in bit_setting::bit_settings::md5_populate() {
            if hex.eq(&hash_result) {
                println!("MD5 :\t{}\nTEST:\t{}", hex, hash_result);
                println!("MD5 Check: PASS");
            }
        }
    }
}
