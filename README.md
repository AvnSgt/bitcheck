# bitcheck

![ArchLABs Logo](https://archlabsblog.files.wordpress.com/2017/09/archlabs_logo.png?w=286&h=286)

**What is bitcheck?** 

- It is a simple rust script that will perform the md5 and sha1 hash check of the ArchLabs ISO images.

**Why** 
- Safety. Nothing is perfect, but rust goes along way to make it more difficult for things like: out-of-bounds reads/writes, use-after-free, null pointer dereference and data races to occur. All of which, can lead to potentially exploitable behavior

**How**
- To use this script, simply install it, with cargo.
```
//Windows Command Line
C:\Users\username>cargo install bitcheck
//Linux Terminal
username@example.home>cargo install bitcheck
//the script will install to ~/.cargo/bin
```
- It is suggested that the **~/.cargo/bin** folder be added to the existing path
- This can be done by adding the following line to the .bashrc, in the home folder
```
export PATH=$PATH:$HOME/.cargo/bin
```
- Save, exit and relaunch the terminal to load the recent changes.
- Bitcheck now supports all versions of ArchLabs currently available for download.
    - Currently, it will only check the first version of ArchLabs found within the directory tree.
        - Suggest remove outdated copies, and only test the current release.
- ~~Currently only **two** version of ***ArchLabs*** can be downloaded.~~
  - ~~**archlabs-2017-10.iso**~~
  - ~~**archlabs-2017-10-lts.iso**~~

__Note__: The script will crawl your home directory tree looking for the ArchLabs images.
- When a potential ArchLabs image is found, it will return the location of it.
    - Added support for Windows Directory Tree structure!

**Running Bitcheck**
```
// In a terminal or the command line for Windows, not PowerShell. Only after bitcheck has been installed using cargo
// Linux Terminal
username@example.home>bitcheck
archlabs-2018-03.iso
"/home/<username>/Downloads/archlabs-2018-03.iso"
SHA1:	68209a4efbc005b56611f15c90cfa287bad6cb2a
TEST:	68209a4efbc005b56611f15c90cfa287bad6cb2a
SHA1 Check: PASS
MD5 :	8a17ea07ecb0381bfbacd66f4dea783c
TEST:	8a17ea07ecb0381bfbacd66f4dea783c
MD5 Check: PASS


//Windows Command Line
C:\Users\username>bitcheck
archlabs-2018-03.iso
"C:\\Users\\username\\Downloads\\archlabs-2018-03.iso"
SHA1:	68209a4efbc005b56611f15c90cfa287bad6cb2a
TEST:	68209a4efbc005b56611f15c90cfa287bad6cb2a
SHA1 Check: PASS
MD5 :	8a17ea07ecb0381bfbacd66f4dea783c
TEST:	8a17ea07ecb0381bfbacd66f4dea783c
MD5 Check: PASS
```
__**Important Note**__
If the one or both of the above is not present, after the location of the image.
The image you have is **CORRUPT!**
Do not use it, as it could have been tampered with.

**Note: .torrent** files should be removed before checking the iso.

**bitcheck** , is only intended for **use** with ***ArchLabs.***

---
bitcheck is an evolving project, and the current code is under going a major overhaul.
Because of this, the direction of the project is bound to change.
